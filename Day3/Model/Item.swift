import Foundation

import ObjectMapper

class Item: Mappable {

   
    // MARK: - Properties

    var content: Content?
    var id: Int = 0
    var date: String?
    var image: String?
    var title: String?

    required init?(map: Map) {}

    // MARK: - Mappable

    func mapping(map: Map) {
        content   <- map["content"]
        id   <- map["id"]
        date   <- map["date"]
        image   <- map["image"]
        title   <- map["title"]
    }
}

class Content: Mappable {

    internal init(description: String? = nil, url:String? = nil)
    {
        self.description = description
        self.url = url
    }
    init(){
        
    }
    // MARK: - Properties

    var description: String?
    var url: String?

    required init?(map: Map) {}

    // MARK: - Mappable

    func mapping(map: Map) {
        description   <- map["description"]
        url   <- map["url"]
    }
}
