// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to Entity.swift instead.

import Foundation
import CoreData

public enum EntityAttributes: String {
    case date = "date"
    case des = "des"
    case id = "id"
    case image = "image"
    case title = "title"
    case url = "url"
}

open class _Entity: NSManagedObject {

    // MARK: - Class methods

    open class func entityName () -> String {
        return "Entity"
    }

    open class func entity(managedObjectContext: NSManagedObjectContext) -> NSEntityDescription? {
        return NSEntityDescription.entity(forEntityName: self.entityName(), in: managedObjectContext)
    }

    @nonobjc
    open class func fetchRequest() -> NSFetchRequest<Entity> {
        return NSFetchRequest(entityName: self.entityName())
    }

    // MARK: - Life cycle methods

    public override init(entity: NSEntityDescription, insertInto context: NSManagedObjectContext?) {
        super.init(entity: entity, insertInto: context)
    }

    public convenience init?(managedObjectContext: NSManagedObjectContext) {
        guard let entity = _Entity.entity(managedObjectContext: managedObjectContext) else { return nil }
        self.init(entity: entity, insertInto: managedObjectContext)
    }

    // MARK: - Properties

    @NSManaged open
    var date: String?

    @NSManaged open
    var des: String?

    @NSManaged open
    var id: Int16 // Optional scalars not supported

    @NSManaged open
    var image: String?

    @NSManaged open
    var title: String?

    @NSManaged open
    var url: String?

    // MARK: - Relationships

}

