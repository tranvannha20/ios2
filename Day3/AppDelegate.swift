//
//  AppDelegate.swift
//  Day3
//
//  Created by Nha on 7/21/20.
//  Copyright © 2020 Nha. All rights reserved.
//

import UIKit
import MagicalRecord
import ESTabBarController_swift


@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    var tabbarController: ESTabBarController!

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        MagicalRecord.setupAutoMigratingCoreDataStack()
        let paths = NSSearchPathForDirectoriesInDomains(FileManager.SearchPathDirectory.documentDirectory, FileManager.SearchPathDomainMask.userDomainMask, true)
        print(paths[0])
        initTabbar()
        return true
    }

   func initTabbar() {
   tabbarController = ESTabBarController()
   let story = UIStoryboard(name: "Main", bundle: nil)
   let newsvc = story.instantiateViewController(withIdentifier: "NewsVC") as! NewsVC
   let bookmarkVC = story.instantiateViewController(withIdentifier: "BookmarkVC") as! BookmarkVC
   let accountVC = story.instantiateViewController(withIdentifier: "AccountVC") as! AccountVC
   
   
   newsvc.tabBarItem = ESTabBarItem(title: "News", image: #imageLiteral(resourceName: "home"), selectedImage: #imageLiteral(resourceName: "home_1"))
   bookmarkVC.tabBarItem = ESTabBarItem(title: "BookMark", image: #imageLiteral(resourceName: "favor"), selectedImage: #imageLiteral(resourceName: "favor_1"))
   accountVC.tabBarItem = ESTabBarItem(title: "Account", image: #imageLiteral(resourceName: "me"), selectedImage: #imageLiteral(resourceName: "me_1"))
   
   let nav1 = UINavigationController(rootViewController: newsvc)
   let nav2 = UINavigationController(rootViewController: bookmarkVC)
   let nav3 = UINavigationController(rootViewController: accountVC)
   
   tabbarController.viewControllers = [nav1, nav2, nav3]
   window?.rootViewController = tabbarController
    }
}
