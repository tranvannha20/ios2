//
//  NewsVC.swift
//  Day3
//
//  Created by Nha on 7/22/20.
//  Copyright © 2020 Nha. All rights reserved.
//

import UIKit
import Alamofire
import ObjectMapper

class NewsVC: UITableViewController {


    
    var listNews = [Item]()
    

    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Trang chủ"
        getData()
        
    }
    
    func  getData(){
        AF.request("\(API_NEWS)", method: .get).responseJSON{(response)in
            switch response.result{
            case .success:
                let model = Mapper<Item>().mapArray(JSONObject: response.value)
                guard let models = model else {
                    return
                }
                self.listNews = models
                DispatchQueue.main.async {
                    self.tableView.reloadData()
                }
                
                break
            case let .failure(err):
                print(err)
                break
                
                
            }
        }
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return listNews.count
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "NewsCell", for: indexPath) as! NewsCell
        let model = listNews[indexPath.row]
        cell.lblDate.text = model.date
        cell.lblTitle.text = model.title
        cell.lblDiscription.text = model.content?.description
        cell.imgCover.kf.setImage(with: URL(string: model.image ?? ""))

        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: false)
        let model = listNews[indexPath.row]
        let story = UIStoryboard(name: "Main", bundle: nil)
        let vc = story.instantiateViewController(identifier: "DetailVC") as! DetailVC
        vc.item = model
        self.navigationController?.pushViewController(vc, animated: true)
    }
   

}
