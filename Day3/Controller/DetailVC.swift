

import UIKit
import WebKit
import MagicalRecord

class DetailVC: UIViewController {

    var item: Item?
    var webView: WKWebView!
    var btBookmark: UIBarButtonItem!
    var isBookmark = false
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = "Chi tiết"
        
        webView = WKWebView()
        view = webView
        let url = "\(SERVER)\(item?.content?.url ?? "")"
        let request = URLRequest(url: URL(string: url)!)
        webView.load(request)
        
        checkBookmark()
        initView()
               
    }
    
    func initView(){
        btBookmark = UIBarButtonItem(image: #imageLiteral(resourceName: "ic_bookmark"), style: .plain, target: self, action: #selector(onBookmark))
        self.navigationItem.rightBarButtonItem = btBookmark
    }
    
    @objc func onBookmark(){
        if isBookmark {
            btBookmark.image = #imageLiteral(resourceName: "ic_bookmark")
            deleteBookmark()
        } else {
            saveBookmark()
            btBookmark.image = #imageLiteral(resourceName: "ic_bookmark_fill")
        }
        isBookmark = !isBookmark
    }
    
    func getAllBookmark() {
        let arr = Entity.mr_findAll() ?? []
        for bookmark in arr {
            if let model = bookmark as? Entity {
                print(model.title ?? "")
              //  print(model.content ?? "")
                print(model.id)
            }
        }
    }
    
    func checkBookmark() {
        MagicalRecord.save({ (context) in
            let bookmark = Entity.mr_findFirst(byAttribute: "id", withValue: self.item?.id ?? 0)
            if bookmark != nil {
                self.isBookmark = true
                self.btBookmark?.image = #imageLiteral(resourceName: "ic_bookmark_fill")
            }else {
                self.isBookmark = false
                self.btBookmark?.image = #imageLiteral(resourceName: "ic_bookmark")
            }
        }) { (success, err) in
            self.getAllBookmark()
        }
    }
    func saveBookmark() {
        MagicalRecord.save({ (context) in
            let bookmark = Entity.mr_createEntity(in: context)
            bookmark?.title = self.item?.title
            bookmark?.date = self.item?.date
            bookmark?.des = self.item?.content?.description
            bookmark?.url = self.item?.content?.url
            let convert = NSNumber(value: self.item?.id ?? 0)
            bookmark?.id = Int16(exactly: convert) ?? 0
            
        }) { (success, error) in
            self.getAllBookmark()
        }
    }
    
    func deleteBookmark() {
        MagicalRecord.save({ (context) in
            
            let bookmark = Entity.mr_findFirst(byAttribute: "id", withValue: self.item?.id ?? 0)
            bookmark?.mr_deleteEntity(in: context)
            
        }) { (success, error) in
            self.getAllBookmark()
        }
    }
   

}
