//
//  ViewController.swift
//  Day3
//
//  Created by Nha on 7/21/20.
//  Copyright © 2020 Nha. All rights reserved.
//

import UIKit
import Alamofire
import ObjectMapper
import Kingfisher

class ViewController: UIViewController {

    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var imgCover: UIImageView!
    @IBOutlet weak var lblContent: UILabel!
    
    var model: Item?
    override func viewDidLoad() {
        super.viewDidLoad()
        getData()
        
    }
    func getData(){
        AF.request("\(API_VC)", method: .get).responseJSON{(response)in
            switch response.result{
            case .success:
                self.model = Mapper<Item>().map(JSONObject: response.value)
                self.setData()
                break
            case let .failure(err):
                print(err)
                break
                
                
            }
        }
        
    }
    func setData(){
        lblDate.text = model?.date ?? ""
        lblTitle.text = model?.title ?? ""
        lblContent.text = model?.content?.description ?? ""
        imgCover.kf.setImage(with: URL(string: model?.image ?? "http://link-image-default" ))
    }

}

