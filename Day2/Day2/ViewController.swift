//
//  ViewController.swift
//  Day2
//
//  Created by Nha on 7/20/20.
//  Copyright © 2020 Nha. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var btnClick: UIButton!
    @IBOutlet weak var lbTitle: UILabel!
    @IBOutlet weak var txtNoidung: UITextField!
    @IBOutlet weak var imgView: UIImageView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initview()
    }
    
    
    func initview(){
        lbTitle.text = "Home"
        lbTitle.textColor = #colorLiteral(red: 0.4666666687, green: 0.7647058964, blue: 0.2666666806, alpha: 1)
        lbTitle.layer.borderWidth = 1
        lbTitle.layer.borderColor = #colorLiteral(red: 0.9529411793, green: 0.6862745285, blue: 0.1333333403, alpha: 1)
        lbTitle.layer.cornerRadius = 5
        btnClick.setTitle("Chon", for: .normal)
        btnClick.setTitleColor(#colorLiteral(red: 0.06274510175, green: 0, blue: 0.1921568662, alpha: 1), for: .normal)
        btnClick.layer.borderWidth = 1
        btnClick.layer.borderColor = #colorLiteral(red: 0.5725490451, green: 0, blue: 0.2313725501, alpha: 1)
        btnClick.layer.cornerRadius = 5

    }
    
    @IBAction func onClick(_ sender: Any) {
        lbTitle.text = "iOS"
        lbTitle.textColor = #colorLiteral(red: 0.4666666687, green: 0.7647058964, blue: 0.2666666806, alpha: 1)
        lbTitle.layer.borderWidth = 1
        lbTitle.layer.borderColor = #colorLiteral(red: 0.5568627715, green: 0.3529411852, blue: 0.9686274529, alpha: 1)
        lbTitle.layer.cornerRadius = 5
        
        
        //Goi storyboard
        
        let story = UIStoryboard(name: "Main", bundle: nil)
        
        let detailVC = story.instantiateViewController(withIdentifier: "DetailDC") as!
        DetailDC
        
        detailVC.username = txtNoidung.text
        
        self.navigationController?.pushViewController(detailVC, animated: true)
    }
    
    func setData(){
               
    }
  
    
}

