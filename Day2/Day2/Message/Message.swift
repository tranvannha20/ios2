//
//  Message.swift
//  Day2
//
//  Created by Nha on 7/21/20.
//  Copyright © 2020 Nha. All rights reserved.
//

import UIKit

class Message: NSObject {
    
    var user: String?
    var time: String?
    var content: String?
    var image: String?

    override init() {
        
    }

    convenience init(user:String, time:String, content: String, image:String) {
        self.init()
        self.user = user
        self.time = time
        self.content = content
        self.image = image
    }
}
