//
//  ProductCell.swift
//  Day2
//
//  Created by Nha on 7/21/20.
//  Copyright © 2020 Nha. All rights reserved.
//

import UIKit

class ProductCell: UICollectionViewCell {
    
    @IBOutlet weak var ivCover: UIImageView!
    @IBOutlet weak var lblProductNam: UILabel!
    @IBOutlet weak var lblPrice: UILabel!
}
