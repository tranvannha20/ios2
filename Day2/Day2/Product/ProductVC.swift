//
//  ProductVC.swift
//  Day2
//
//  Created by Nha on 7/21/20.
//  Copyright © 2020 Nha. All rights reserved.
//

import UIKit

private let reuseIdentifier = "ProductCell"

class ProductVC: UICollectionViewController {

    var listProduct = [Product]()
    override func viewDidLoad() {
        super.viewDidLoad()
        initData()


       
    }

   func initData(){
       for i in 1...8{
        let prduct = Product(image: "ic_p\(i).jpg", productname: "Dam \(i)", price: 500000)
        listProduct.append(prduct)
        }
    }
    // MARK: UICollectionViewDataSource

    override func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }


    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return listProduct.count
    }

    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath) as! ProductCell
        let model = listProduct[indexPath.row]
        
        cell.ivCover.image = UIImage(named: model.ivCover ?? "ic_p1.jpg")
        cell.lblProductNam.text = model.productname
        cell.lblPrice.text = "\(model.price)"
    
        return cell
    }

    // MARK: UICollectionViewDelegate

    /*
    // Uncomment this method to specify if the specified item should be highlighted during tracking
    override func collectionView(_ collectionView: UICollectionView, shouldHighlightItemAt indexPath: IndexPath) -> Bool {
        return true
    }
    */

    /*
    // Uncomment this method to specify if the specified item should be selected
    override func collectionView(_ collectionView: UICollectionView, shouldSelectItemAt indexPath: IndexPath) -> Bool {
        return true
    }
    */

    /*
    // Uncomment these methods to specify if an action menu should be displayed for the specified item, and react to actions performed on the item
    override func collectionView(_ collectionView: UICollectionView, shouldShowMenuForItemAt indexPath: IndexPath) -> Bool {
        return false
    }

    override func collectionView(_ collectionView: UICollectionView, canPerformAction action: Selector, forItemAt indexPath: IndexPath, withSender sender: Any?) -> Bool {
        return false
    }

    override func collectionView(_ collectionView: UICollectionView, performAction action: Selector, forItemAt indexPath: IndexPath, withSender sender: Any?) {
    
    }
    */

}
