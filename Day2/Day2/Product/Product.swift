//
//  Product.swift
//  Day2
//
//  Created by Nha on 7/21/20.
//  Copyright © 2020 Nha. All rights reserved.
//

import UIKit

class Product: NSObject {

    var ivCover : String?
    var productname: String?
    var price: Int = 0

    override init(){
        
    }
    
    convenience init(image: String, productname:String,price:Int) {
        self.init()
        self.ivCover = image
        self.productname = productname
        self.price = price    }
}
